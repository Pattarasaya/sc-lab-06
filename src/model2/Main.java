package model2;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grizzly_Lion grizzy = new Grizzly_Lion("Mint");
		Snack  snack = new Snack("Waew");
		Tiger polar = new Tiger("Kate");
		
		Eating_Lion eating = new Eating_Lion();
		System.out.println(eating.makeBearEat(grizzy));
		System.out.println(eating.makeBearEat(snack));
		System.out.println(eating.makeBearEat(polar));
		System.out.println(eating.makeBearEat((Lion) polar));
	}


}
