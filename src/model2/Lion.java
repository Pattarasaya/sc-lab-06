package model2;

public abstract class Lion {
	private String name;
	
	public Lion(String name){
		this.name = name;
	}
	
	public abstract String eat();
	
	public String toString(){
		return this.name;
	}

}
