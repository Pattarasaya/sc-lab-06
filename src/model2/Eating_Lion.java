package model2;

public class Eating_Lion {
	public String makeBearEat(Snack snack){
		return "Snake: "+snack.eat();
	}
	public String makeBearEat(Tiger tiger){
		return "Tiger: "+tiger.eat();
	}
	public String makeBearEat(Grizzly_Lion grizzy){
		return "Grizzy: "+grizzy.eat();
	}
	public String makeBearEat(Lion lion){
		return "Lion: "+lion.eat();
	}

}
